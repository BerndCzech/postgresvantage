package db

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

// Query the database for the information requested and prints the results.
// If the query fails exit the program with an error.
// func queryNextShare(ctx context.Context, parameter int)
func (a *app) queryNextShare(ctx context.Context) (string, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	//err := a.QueryRowContext(ctx, "select p.name from people as p where p.id = :id;", sql.Named("id", id)).Scan(&name)
	rows, err := a.db.Query(ctx, `
		SELECT symbol from staticdata.shares
		where isvalid=true
		order by  lastrefreshed asc,shareid
		limit 1`)
	if err != nil {
		return "", fmt.Errorf("connect: %w", err)
	}

	defer rows.Close()
	var resSymbol string
	for rows.Next() {

		err = rows.Scan(&resSymbol)
	}
	if err != nil {
		return "", fmt.Errorf("connect: %w", err)
	}
	return resSymbol, nil

}

func (a *app) edit(ctx context.Context, d dataTimeSeries) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	// 8 fields from single Day struct + date
	numUpdatedFields := 8 + 1

	// symbol is $1
	valueStrings := make([]string, 0, 1+len(d.TimeSeries))
	valueArgs := make([]interface{}, 0, 1+len(d.TimeSeries)*numUpdatedFields)

	insertMarketData := `
	WITH myconstants (shareid) as (
		SELECT shareid from  staticdata.shares where symbol = $1 and isvalid = true
		),
		updated as (
		update  staticdata.shares
		set lastrefreshed  = cast( now() at time zone 'utc' as date)
		where symbol = $1 and isvalid = true
	)

	INSERT INTO dynamicdata.shares(
		evaldate,shareid,priceclosing,pricehigh,pricelow,priceopening,volume,PriceAdjustedClose,DividendAmount,SplitCoefficient
		) VALUES %s
		ON CONFLICT(evaldate,shareid) DO UPDATE
		set priceopening=EXCLUDED.priceopening,priceclosing=EXCLUDED.priceclosing,pricehigh=EXCLUDED.pricehigh,
		pricelow=EXCLUDED.pricelow,volume=EXCLUDED.volume,PriceAdjustedClose=EXCLUDED.PriceAdjustedClose,DividendAmount=EXCLUDED.DividendAmount,SplitCoefficient=EXCLUDED.SplitCoefficient;`

	// symbol =$1
	// parse Symbol to query
	valueArgs = append(valueArgs, d.MetaInfo.Symbol)
	i := 2

	// TODO: this is actually wrong as null would be more appropriate for missing
	//  Replace
	noVal := func(v string) string {
		if v == "" {
			return "0"
		}
		return v
	}

	for key, post := range d.TimeSeries {

		valueStrings = append(valueStrings, fmt.Sprintf("($%d, (select shareid from  myconstants) ,$%d,$%d,$%d,$%d,$%d,$%d,$%d,$%d)", i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8))
		valueArgs = append(valueArgs, key)
		valueArgs = append(valueArgs, noVal(post.Close))
		valueArgs = append(valueArgs, noVal(post.High))
		valueArgs = append(valueArgs, noVal(post.Low))
		valueArgs = append(valueArgs, noVal(post.Open))
		valueArgs = append(valueArgs, noVal(post.Volume))
		valueArgs = append(valueArgs, noVal(post.AdjustedClose))
		valueArgs = append(valueArgs, noVal(post.DividendAmount))
		valueArgs = append(valueArgs, noVal(post.SplitCoefficient))
		i += numUpdatedFields
	}

	stmt := fmt.Sprintf(insertMarketData, strings.Join(valueStrings, ","))

	res, err := a.db.Exec(ctx, stmt, valueArgs...)
	if err != nil {
		return fmt.Errorf("connect: %w", err)
	}
	nRows := res.RowsAffected()

	log.SetOutput(os.Stdout)
	log.Printf("Inserted/Updated: %d rows for %s.", nRows, d.MetaInfo.Symbol)
	return nil

}

// PingDB the database to verify DSN provided by the user is valid and the
// server accessible. If the ping fails exit the program with an error.
func (a *app) PingDB(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	countTrial := 1
	if err := a.db.Ping(ctx); err != nil {
		countTrial++
		if countTrial > 5 {
			log.Fatalf("%d Trials failed unable to connect to database: %s", countTrial, err)
		}
		time.Sleep(5 * time.Second)
	}

}

func (a *app) markSymbolInvalid(ctx context.Context, StockSymbol string) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	deactivateSymbol := `
	update staticdata.shares 
	set isvalid =false,lastrefreshed  = cast( now() at time zone 'utc' as date)
	where isvalid = true and symbol = $1`

	res, err := a.db.Exec(ctx, deactivateSymbol, StockSymbol)
	if err != nil {
		return fmt.Errorf("connect: %w", err)
	}
	nRows := res.RowsAffected()

	log.SetOutput(os.Stdout)
	log.Printf("Deactivated %d shares with symbol: %s.", nRows, StockSymbol)
	return &TemporaryError{
		RetryValid: true,
		Err:        fmt.Errorf("Stock marked as invalid: %s", StockSymbol),
	}

}
