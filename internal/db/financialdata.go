package db

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
)

// AssetExtractor describes financial data relation
type AssetExtractor struct {
	db PgxIface
}

type PgxIface interface {
	Ping(ctx context.Context) error
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
}

func NewAssetExtractor(db PgxIface) *AssetExtractor {
	return &AssetExtractor{db: db}
}

// HealthCheck currently only pings the db to see if it is available
func (a *AssetExtractor) HealthCheck(ctx context.Context) <-chan error {
	errChan := make(chan error, 2)
	go func() {
		// safe to close it as the only sender (see output direction)
		defer close(errChan)
		update5sec := 5 * time.Second
		tick := time.Tick(update5sec)
		for {
			select {
			case <-ctx.Done():
				errChan <- ctx.Err()
				break
			case <-tick:
			}
			ctxChild, cancel := context.WithTimeout(ctx, update5sec)
			if err := a.db.Ping(ctxChild); err != nil {
				errChan <- err
			}
			cancel()
		}
	}()
	return errChan
}

func (a *AssetExtractor) AssetsByID(ctx context.Context, ids ...AssetID) ([]PricedAsset, error) {

	pas := make([]PricedAsset, len(ids))
	// TODO(2): replace with single query for all ids
	for i, id := range ids {
		pa, err := a.assetByID(ctx, id)
		if err != nil {
			return nil, err
		}
		pas[i] = pa
	}
	return pas, nil
}

// AssetsBySearch provides basic info for searched assets.
// It sorts (highest first) by traded volume of the last daysPast.
// func (a *AssetExtractor) GetShareInfo(ctx context.Context, shareIDs []int, daysPast int, queryString string) ([]PricedAsset, error) {
func (a *AssetExtractor) AssetsBySearch(ctx context.Context, search string, MinAge time.Time, Active bool) ([]PricedAsset, error) {
	stmt :=
		// language=PostgreSQL
		`with last30days as (select shareid,
                           volume,
                           COALESCE(priceclosing, 0)                                  as price,
                           rank() over (Partition by shareid order by evaldate desc ) as day
                    from dynamicdata.shares
                    where evaldate > CURRENT_DATE - 30 * interval '1 day'
                      and shareid in (select shares.shareid
                                      from staticdata.shares
                                      where (sharename ILIKE '%' || $4 || '%' or symbol ILIKE '%' || $5 || '%')
                                        and active = $1
                                        and isvalid = $2
                                        -- must have that record history  
                                        and exists(select
                                                   from dynamicdata.shares ds
                                                   where shares.shareid = ds.shareid
                                                     and ds.evaldate <= $3))),
     volumeorder as (select shareid,
                            avg(volume * price) as avgvolume,
                            max(
                                    case
                                        --get recent price
                                        When day = 1 then
                                            price
                                        Else
                                            -1
                                        end
                                )               as price
                     from last30days
                     group by shareid)

select vo.shareid,
       sd.sharename,
       sd.symbol,
       vo.price
from volumeorder vo
         inner join staticdata.shares sd on vo.shareid = sd.shareid
order by vo.avgvolume desc
limit 20 `

	rows, err := a.db.Query(ctx, stmt,
		Active, // staticdata.active
		Active, // staticdata.isvalid
		MinAge,
		search,
		search,
	)
	defer rows.Close()
	if err != nil {
		return nil, errors.Errorf("connect: %+v", err)
	}

	var assets []PricedAsset

	for rows.Next() {
		asset := PricedAsset{}

		if err := rows.Scan(&asset.ID, &asset.Name, &asset.Symbol, &asset.Price); err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, errors.WithStack(ErrNoRecord)
			}
			return nil, errors.WithStack(err)
		}
		assets = append(assets, asset)
	}
	// When the rows.Next() loop has finished we call rows.Err() to retrieve any
	// error that was encountered during the iteration. It's important to
	// call this - don't assume that a successful iteration was completed
	// over the whole assetsultset.
	return assets, rows.Err()
}

// Assets provides basic info for searched assets.
// It sorts (highest first) by traded volume of the last days past.
func (a *AssetExtractor) Assets(ctx context.Context, MinAge time.Time, Active bool) ([]PricedAsset, error) {
	stmt :=
		// language=PostgreSQL
		`
	with matured as (
	select distinct shareid
	from dynamicdata.shares
	where evaldate <= $1
	),
	latest as (
	  select shareid, max(evaldate) as date
	  from dynamicdata.shares
	  where shares.shareid in (select shareid from matured)
	  group by shareid
	)
	select shares.shareid, sharename, symbol, priceclosing
	from staticdata.shares
			 inner join dynamicdata.shares ds on shares.shareid = ds.shareid
			 inner join latest on latest.shareid = ds.shareid and latest.date = ds.evaldate
	where shares.active = $2 and shares.isvalid = $3;
	`
	rows, err := a.db.Query(ctx, stmt, MinAge, Active, Active)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	var assets []PricedAsset
	for rows.Next() {
		asset := PricedAsset{}
		if err := rows.Scan(&asset.ID, &asset.Name, &asset.Symbol, &asset.Price); err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, errors.WithStack(ErrNoRecord)
			}
			return nil, errors.WithStack(err)
		}
		assets = append(assets, asset)
	}

	return assets, nil
}

// TimeSeriesAssetsByID any return asset comes with a timeSeries starting at `from`
func (a *AssetExtractor) TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...AssetID) ([]TimeSeriesAsset, error) {
	ts := make([]TimeSeriesAsset, len(ids))
	for i, id := range ids {
		t, err := a.timeSeriesAssetByID(ctx, from, id)
		if err != nil {
			return nil, err
		}
		asset, err := a.assetByID(ctx, id)
		if err != nil {
			return nil, err
		}

		ts[i] = TimeSeriesAsset{
			Asset: Asset{
				ID:     asset.ID,
				Name:   asset.Name,
				Symbol: asset.Symbol,
			},
			TimeSeries: t,
		}
	}

	return ts, nil
}

func (a *AssetExtractor) assetByID(ctx context.Context, id AssetID) (PricedAsset, error) {
	stmt :=
		// language=PostgreSQL
		`
		select shares.shareid, sharename, symbol, priceclosing
		from staticdata.shares
		left join dynamicdata.shares ds on shares.shareid = ds.shareid
		where shares.shareid = $1
		order by evaldate desc
		limit 1;
		`
	var pa PricedAsset
	if err := a.db.QueryRow(ctx, stmt, id).Scan(
		&pa.ID,
		&pa.Name,
		&pa.Symbol,
		&pa.Price,
	); err != nil {
		return PricedAsset{}, err
	}
	return pa, nil
}

func (a *AssetExtractor) timeSeriesAssetByID(ctx context.Context, from time.Time, id AssetID) (TimeSeries, error) {
	stmt :=
		// language=PostgreSQL
		`
		select evaldate, priceclosing
		from dynamicdata.shares 
		where shares.shareid = $1 and evaldate >= $2;
		`

	rows, err := a.db.Query(ctx, stmt, id, from)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	ts := make(TimeSeries)
	for rows.Next() {
		var (
			date  time.Time
			price float64
		)
		if err := rows.Scan(&date, &price); err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, errors.WithStack(ErrNoRecord)
			}
			return nil, errors.WithStack(err)
		}
		ts[date] = price
	}

	return ts, rows.Err()
}
