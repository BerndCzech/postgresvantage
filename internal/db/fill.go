// Package db handles all data exchange
// to the db.
package db

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Config struct {
	PGUser        string `envconfig:"POSTGRES_USER"`
	PGPW          string `envconfig:"POSTGRES_PASSWORD"`
	PGDB          string `envconfig:"POSTGRES_DB"`
	AlphaKey      string `envconfig:"ALPHA_KEY"`
	AlphaFunction string `envconfig:"ALPHA_FUNCTION"`
}

type dataSource interface {
	Pull(ctx context.Context, shareCode string) ([]byte, error)
}
type app struct {
	db *pgxpool.Pool
	Config
	ds dataSource
}

func NewApp(ctx context.Context, c Config) (*app, func(), error) {
	con, err := pgxpool.Connect(
		ctx,
		fmt.Sprintf("postgresql://%s:%s@postgrs:5432/%s?sslmode=disable", c.PGUser, c.PGPW, c.PGDB),
	)
	if err != nil {
		return nil, nil, err
	}
	ds := NewAlphaVantage(c.AlphaFunction, c.AlphaKey)
	return &app{con, c, ds}, con.Close, nil
}

// TemporaryError says that a instant rerun is sensible.
type TemporaryError struct {
	RetryValid bool
	Err        error
}

func (e *TemporaryError) Error() string {
	if !e.RetryValid {
		return fmt.Sprintf("Difficult (non-temporary) issue with data or alphaVantage: %s ", e.Err)
	}
	return fmt.Sprintf("Temporary issue with data or alphaVantage: %s ", e.Err)
}

//grouped type declaration for JSON doc.
type (
	dataTimeSeries struct {
		MetaInfo   metaData             `json:"Meta Data"`
		TimeSeries map[string]singleDay `json:"Time Series (Daily)"`
	}

	metaData struct {
		Information string `json:"1. Information"`
		Symbol      string `json:"2. Symbol"`
		LastRefresh string `json:"3. Last Refreshed"`
		OutputSize  string `json:"4. Output Size"`
		TimeZone    string `json:"5. Time Zone"`
	}
	singleDay struct {
		Open             string `json:"1. open"`
		High             string `json:"2. high"`
		Low              string `json:"3. low"`
		Close            string `json:"4. close"`
		AdjustedClose    string `json:"5. adjusted close"`
		Volume           string `json:"6. volume"`
		DividendAmount   string `json:"7. dividend amount"`
		SplitCoefficient string `json:"8. split coefficient"`
	}
)

func writeErr(err error) error {
	return fmt.Errorf("fill: %+v", err)
}

// Fill puts marketdata into the DB.
func (a app) Fill(ctx context.Context, ShareCodes ...string) error {

	// Assure DB is available
	a.PingDB(ctx)

	// read from database
	readData, err := a.getMarketdata(ctx, ShareCodes...)
	if err != nil {
		log.Printf("temporary error with response: \n%+v\n", readData)
		return &TemporaryError{RetryValid: true, Err: err}
	}

	stockDataStruct := dataTimeSeries{}
	if err := json.Unmarshal(readData, &stockDataStruct); err != nil {
		log.Println(fmt.Sprintf("Problem unmarshalling that json: \n%v", readData))
		return &TemporaryError{RetryValid: true, Err: err}
	}

	// Either wrong Symbol or free access limit reached
	if stockDataStruct.TimeSeries == nil {
		return errors.New("no time-series data provided")
	}

	// update from database
	err = a.edit(ctx, stockDataStruct)
	if err != nil {
		return writeErr(err)
	}

	return nil
}

// getMarketdata gets data from alphaventage
// If ShareCodes is left empty a Symbol will be auto-picked
func (a *app) getMarketdata(ctx context.Context, ShareCodes ...string) ([]byte, error) {
	var (
		err       error
		ShareCode string
	)
	switch givenShareCodes := len(ShareCodes); {
	case givenShareCodes == 0 || givenShareCodes == 1 && ShareCodes[0] == "":
		ShareCode, err = a.queryNextShare(ctx)
		if err != nil {
			return nil, fmt.Errorf("GetMarketdata: Could not find next share: %+v", err)
		}
	case givenShareCodes == 1:
		ShareCode = ShareCodes[0]
	default:
		return nil, fmt.Errorf("GetMarketdata: more then 1 Symbol is not yet supported")
	}

	respBody, err := a.ds.Pull(ctx, ShareCode)
	if err != nil {
		switch errors.Cause(err) {
		case ErrCode:
			log.Printf("no new data: %+v", err)
			return nil, a.markSymbolInvalid(ctx, ShareCode)
		case ErrLimit:
			log.Printf("no new data: %+v", err)
			ticker := time.NewTicker(4 * time.Hour)
			defer ticker.Stop()
			for {
				select {
				case <-ctx.Done():
					return nil, ctx.Err()
				case <-ticker.C:
					return a.getMarketdata(ctx, ShareCode)
				}
			}
		default:
			return nil, err
		}
	}
	return respBody, nil

}
