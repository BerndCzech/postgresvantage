package db

import (
	"time"

	"github.com/pkg/errors"
)

// ErrNoRecord is returned if the query is empty.
var ErrNoRecord = errors.New("models: no matching record found")

type AssetID = int

// Asset is the base struct for assets.
type Asset struct {
	ID     AssetID `json:"shareid"`
	Name   string  `json:"description"`
	Symbol string  `json:"symbol"`
}

// PricedAsset extends Asset by the latest price for value orientation.
type PricedAsset struct {
	Asset
	Price float64 `json:"price"`
}

// TimeSeriesAsset extends Asset by a TimeSeries to do computation.
type TimeSeriesAsset struct {
	Asset
	TimeSeries TimeSeries `json:"time_series"`
}

// TimeSeries maps SharePrice per day (=unique key)
type TimeSeries map[time.Time]float64
