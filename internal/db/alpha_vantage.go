package db

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

var (
	ErrLimit = errors.New("daily limit reached")
	ErrCode  = errors.New("invalid code requested")
)

type AlphaVantage struct {
	alphaFunction string
	alphaKey      string
}

func NewAlphaVantage(alphaFunction,
	alphaKey string) AlphaVantage {
	return AlphaVantage{
		alphaFunction: alphaFunction,
		alphaKey:      alphaKey,
	}
}

func (s AlphaVantage) Pull(ctx context.Context, ShareCode string) ([]byte, error) {

	log.Printf("Requesting marketdata for %s\n", ShareCode)

	MyKey := s.alphaKey
	const APIURL = "https://www.alphavantage.co/query"

	client := &http.Client{}

	req, err := http.NewRequestWithContext(ctx, "GET", APIURL, nil)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	q := req.URL.Query()
	q.Add("function", s.alphaFunction) //"TIME_SERIES_DAILY")
	q.Add("symbol", ShareCode)
	q.Add("outputsize", "full")
	q.Add("datatype", "json")
	q.Add("apikey", MyKey)

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("GetMarketdata: Errored when sending request to the server: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetMarketdata: Non 200 StatusCode: %d", resp.StatusCode)
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	switch apiAnswer := string(respBody); {
	case strings.Contains(apiAnswer, "Invalid API call"):
		return nil, ErrCode
	case strings.Contains(apiAnswer, "500 requests/day"):
		return nil, ErrLimit
	case !strings.Contains(apiAnswer, "open"):
		log.Printf("response does not contain open - there could be something wrong:\n%s", apiAnswer)
	default:
		// fine response
	}
	return respBody, nil
}
