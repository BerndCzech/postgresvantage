package db

import (
	"context"
	"testing"
	"time"

	"github.com/pashagolub/pgxmock"
	"github.com/stretchr/testify/assert"
)

func TestAssetExtractor_Assets(t *testing.T) {
	mock, err := pgxmock.NewConn()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer mock.Close(context.Background())

	a := NewAssetExtractor(mock)
	type args struct {
		MinAge time.Time
		Active bool
		rows   *pgxmock.Rows
	}
	tests := []struct {
		name    string
		args    args
		want    []PricedAsset
		wantErr bool
	}{
		{name: "all assets",
			args: args{
				MinAge: time.Time{}, Active: true,
				rows: pgxmock.NewRows([]string{"shareid", "sharename", "symbol", "priceclosing"}).AddRow(123, "Microsoft CORP.", "MSFT", 100.7)},
			want: []PricedAsset{{
				Asset: Asset{ID: 123, Name: "Microsoft CORP.", Symbol: "MSFT"},
				Price: 100.7},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectQuery("select").WithArgs(
				tt.args.MinAge,
				tt.args.Active,
				tt.args.Active,
			).WillReturnRows(tt.args.rows)
			got, err := a.Assets(context.Background(), tt.args.MinAge, tt.args.Active)
			if (err != nil) != tt.wantErr {
				t.Errorf("%+v", err)
			}
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestAssetExtractor_AssetsByID(t *testing.T) {
	mock, err := pgxmock.NewConn()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer mock.Close(context.Background())

	a := NewAssetExtractor(mock)
	type args struct {
		id   []AssetID
		rows *pgxmock.Rows
	}
	tests := []struct {
		name    string
		args    args
		want    []PricedAsset
		wantErr bool
	}{
		{name: "MSFT by ID",
			args: args{
				id:   []AssetID{123},
				rows: pgxmock.NewRows([]string{"shareid", "sharename", "symbol", "priceclosing"}).AddRow(123, "Microsoft CORP.", "MSFT", 100.7)},
			want: []PricedAsset{{
				Asset: Asset{ID: 123, Name: "Microsoft CORP.", Symbol: "MSFT"},
				Price: 100.7}},
			wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectQuery("select").WithArgs(
				tt.args.id[0],
			).WillReturnRows(tt.args.rows)
			got, err := a.AssetsByID(context.Background(), tt.args.id...)
			if (err != nil) != tt.wantErr {
				t.Errorf("%+v", err)
			}
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestAssetExtractor_AssetsBySearch(t *testing.T) {
	mock, err := pgxmock.NewConn()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer mock.Close(context.Background())

	a := NewAssetExtractor(mock)
	type args struct {
		Search string
		MinAge time.Time
		Active bool
		rows   *pgxmock.Rows
	}
	tests := []struct {
		name    string
		args    args
		want    []PricedAsset
		wantErr bool
	}{
		{name: "Search Microsoft",
			args: args{
				MinAge: time.Time{}, Active: true, Search: "msft",
				rows: pgxmock.NewRows([]string{"shareid", "sharename", "symbol", "priceclosing"}).AddRow(123, "Microsoft CORP.", "MSFT", 100.7)},
			want: []PricedAsset{{
				Asset: Asset{ID: 123, Name: "Microsoft CORP.", Symbol: "MSFT"},
				Price: 100.7},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectQuery("select").WithArgs(
				tt.args.Active,
				tt.args.Active,
				tt.args.MinAge,
				tt.args.Search,
				tt.args.Search,
			).WillReturnRows(tt.args.rows)
			got, err := a.AssetsBySearch(context.Background(), tt.args.Search, tt.args.MinAge, tt.args.Active)
			if (err != nil) != tt.wantErr {
				t.Errorf("%+v", err)
			}
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestAssetExtractor_TimeSeriesAssetsByID(t *testing.T) {
	mock, err := pgxmock.NewConn()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer mock.Close(context.Background())

	a := NewAssetExtractor(mock)
	type args struct {
		id   []AssetID
		from time.Time
		rows []*pgxmock.Rows
	}
	tests := []struct {
		name    string
		args    args
		want    []TimeSeriesAsset
		wantErr bool
	}{
		{name: "MSFT timeseries",
			args: args{
				id:   []AssetID{123},
				from: time.Time{},
				rows: []*pgxmock.Rows{
					// Asset Query
					pgxmock.NewRows([]string{"shareid", "sharename", "symbol", "priceclosing"}).AddRow(123, "Microsoft CORP.", "MSFT", 100.7),
					// TimeSeries Query
					pgxmock.NewRows([]string{"evaldate", "priceclosing"}).
						AddRow(time.Time{}.Add(24*60*time.Minute), 100.7).
						AddRow(time.Time{}.Add(2*24*60*time.Minute), 101.7),
				}},
			want: []TimeSeriesAsset{{
				Asset: Asset{ID: 123, Name: "Microsoft CORP.", Symbol: "MSFT"},
				TimeSeries: TimeSeries{
					time.Time{}.Add(24 * 60 * time.Minute):     100.7,
					time.Time{}.Add(2 * 24 * 60 * time.Minute): 101.7}}},
			wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// the order the sql statements are called is important
			mock.ExpectQuery("from dynamicdata.shares").WithArgs(
				tt.args.id[0], tt.args.from,
			).WillReturnRows(tt.args.rows[1])
			mock.ExpectQuery("from staticdata.shares").WithArgs(
				tt.args.id[0],
			).WillReturnRows(tt.args.rows[0])
			got, err := a.TimeSeriesAssetsByID(context.Background(), tt.args.from, tt.args.id...)
			if (err != nil) != tt.wantErr {
				t.Errorf("%+v", err)
			}
			assert.Equal(t, tt.want, got)
		})
	}
}
