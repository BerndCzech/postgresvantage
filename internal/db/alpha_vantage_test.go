package db

import (
	"context"
	"os"
	"testing"
)

func TestAlphaVantage_Pull(t *testing.T) {
	type fields struct {
		alphaFunction string
		alphaKey      string
	}

	tests := []struct {
		name      string
		fields    fields
		ShareCode string
		wantErr   bool
	}{
		// TODO: Add test cases.
		{name: "test it",
			fields: fields{
				alphaFunction: "TIME_SERIES_DAILY_ADJUSTED",
				alphaKey:      "",
			}, ShareCode: "CEI",
			wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := AlphaVantage{
				alphaFunction: tt.fields.alphaFunction,
				alphaKey:      tt.fields.alphaKey,
			}
			got, err := s.Pull(context.Background(), tt.ShareCode)
			// check here
			os.WriteFile("./debug.log", got, 0666)

			_ = got
			if tt.wantErr != (err != nil) {
				return
			}

		})
	}
}
