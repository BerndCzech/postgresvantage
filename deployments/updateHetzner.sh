#!/bin/zsh
deployment_path="deployments"

docker buildx build --file build/Dockerfile --target run --tag berndczech/postgresvantage --platform linux/amd64 . --push &&
  rsync -rPhu --stats $deployment_path/init.sql $deployment_path/docker-compose.yaml $deployment_path/.env root@hetzner:/root/ts-vantage &&
  ssh root@hetzner 'docker pull berndczech/postgresvantage; docker-compose -f ts-vantage/docker-compose.yaml up -d'
