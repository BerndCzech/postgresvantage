// Package client provides user interaction. Convenience methods are functional and have tests.
package client

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/BerndCzech/postgresvantage/internal/db"
)

type archive struct {
	source  dataProvider
	options options
}

type (
	AssetID         = db.AssetID
	TimeSeries      = db.TimeSeries
	PricedAsset     = db.PricedAsset
	TimeSeriesAsset = db.TimeSeriesAsset
)

type dataProvider interface {
	AssetsByID(ctx context.Context, ids ...AssetID) ([]PricedAsset, error)
	AssetsBySearch(ctx context.Context, search string, MinAge time.Time, Active bool) ([]PricedAsset, error)
	Assets(ctx context.Context, MinAge time.Time, Active bool) ([]PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...AssetID) ([]TimeSeriesAsset, error)
}
type options struct {
	MinAge time.Time
	Active bool
}

type Option func(*archive)

// WithMinAge filters result set of assets that have no records before `MinAge`
func WithMinAge(MinAge time.Time) func(*archive) {
	return func(a *archive) {
		a.options.MinAge = MinAge
	}
}

// WithActive filters all assets that are only historically listed - i.e. Mostly not at the stock market anymore.
func WithActive(Active bool) func(*archive) {
	return func(a *archive) {
		a.options.Active = Active
	}
}

// NewArchive context and pool are mandatory.
func NewArchive(ctx context.Context, logger *log.Logger, pool *pgxpool.Pool, options ...Option) (*archive, <-chan error) {
	if pool == nil || ctx == nil {
		return nil, nil
	}
	if logger == nil {
		logger = log.New(os.Stdout, "", log.LstdFlags)
	}

	s := db.NewAssetExtractor(pool)
	a := &archive{source: s}

	withDefaultOptions(a)
	for _, option := range options {
		// custom options
		option(a)
	}
	return a, s.HealthCheck(ctx)
}

func withDefaultOptions(a *archive) {
	a.options.MinAge = time.Now().Add(-24 * 200 * time.Hour)
	a.options.Active = true
}

// Assets returns all PricedAssets. If `matured` > 0 only assets with a track history of at least that
// are returned. Thus this can be used
func (a *archive) Assets(ctx context.Context) ([]PricedAsset, error) {
	return a.source.Assets(ctx, a.options.MinAge, a.options.Active)
}

func (a *archive) AssetsByID(ctx context.Context, ids ...AssetID) ([]PricedAsset, error) {
	return a.source.AssetsByID(ctx, ids...)
}

func (a *archive) AssetsBySearch(ctx context.Context, search string) ([]PricedAsset, error) {
	return a.source.AssetsBySearch(ctx, search, a.options.MinAge, a.options.Active)
}

// TimeSeriesAssetsByID returns a timeSeries which starts earliest at `from`.
func (a *archive) TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...AssetID) ([]TimeSeriesAsset, error) {
	return a.source.TimeSeriesAssetsByID(ctx, from, ids...)
}
