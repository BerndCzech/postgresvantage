package client

import (
	"github.com/pkg/errors"
)

// SyncDates drops the days that dont exist for both assets
// If y is nil nothing is done
func SyncDates(x TimeSeries, y TimeSeries) {
	if y == nil || len(y) == 0 {
		return
	}

	for k := range x {
		_, ok := y[k]
		if !ok {
			delete(x, k)
		}
	}
	for k := range y {
		_, ok := x[k]
		if !ok {
			delete(y, k)
		}
	}
}

// Add adds many TimeSeries. Dates that don't exist for all series are dropped.
// If weights are nil they are set = 1.
// Weights don't need to be normalized.
func Add(weights []float64, tslist ...TimeSeries) (TimeSeries, error) {

	combinedSeries := make(TimeSeries)
	switch {
	case weights != nil && len(weights) != len(tslist):
		return nil, errors.Errorf("tsAdd: timeseries list and weights of different length")
	case weights == nil:
		weights = make([]float64, len(tslist))
		for i := range tslist {
			weights[i] = 1
			i++
		}
		// default weights =! nil && len(weights) == len(tslist)
	}

	// loop timeseries
	for i, v := range tslist {

		switch {
		// initialize 1st timeseries
		case len(combinedSeries) == 0:

			switch {
			case weights[i] == 1:
				combinedSeries = copyTS(v)

			default:
				for k := range v {
					combinedSeries[k] = v[k] * weights[i]
				}

			}

		default:
			for k := range combinedSeries {
				// remove date if it doesn't exist
				_, ok := v[k]
				if !ok {
					delete(combinedSeries, k)
					continue
				}

				combinedSeries[k] += v[k] * weights[i]

			}
		}
	}

	return combinedSeries, nil
}

// copyTS returns a timeseries copy by value
func copyTS(ts TimeSeries) TimeSeries {
	cp := make(TimeSeries, len(ts))
	for i, v := range ts {
		cp[i] = v
	}
	return cp
}
