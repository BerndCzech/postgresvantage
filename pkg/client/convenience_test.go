package client

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/BerndCzech/postgresvantage/internal/db"

	"github.com/stretchr/testify/assert"
)

const layoutISO = "2006-01-02"

func TestAdd(t *testing.T) {
	type args struct {
		weights []float64
		tslist  []db.TimeSeries
	}
	tests := []struct {
		name    string
		args    args
		want    db.TimeSeries
		wantErr bool
	}{
		{name: "add 2 time series", args: struct {
			weights []float64
			tslist  []db.TimeSeries
		}{
			weights: nil,
			tslist: []db.TimeSeries{
				map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 1, toTime(t, "2020-01-03"): 0},
				map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 3, toTime(t, "2020-01-03"): 1},
			}},
			want: map[time.Time]float64{toTime(t, "2020-01-01"): 2.4, toTime(t, "2020-01-02"): 4, toTime(t, "2020-01-03"): 1}, wantErr: false},
		{name: "average of 2 time series", args: struct {
			weights []float64
			tslist  []db.TimeSeries
		}{
			weights: []float64{0.5, 0.5},
			tslist: []db.TimeSeries{
				map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 1, toTime(t, "2020-01-03"): 0},
				map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 3, toTime(t, "2020-01-03"): 1},
			}},
			want: map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 2, toTime(t, "2020-01-03"): 0.5}, wantErr: false},
		{name: "too many weights", args: struct {
			weights []float64
			tslist  []db.TimeSeries
		}{
			weights: []float64{1, 1, 1},
			tslist: []db.TimeSeries{
				map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 1, toTime(t, "2020-01-03"): 0},
				map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 3, toTime(t, "2020-01-03"): 1},
			}},
			want: nil, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Add(tt.args.weights, tt.args.tslist...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Add() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func toTime(t *testing.T, s string) time.Time {
	date, err := time.Parse(layoutISO, s)
	if err != nil {
		t.Fatalf("%v", err)
	}
	return date
}

func TestSyncDates(t *testing.T) {
	type args struct {
		x db.TimeSeries
		y db.TimeSeries
	}
	tests := []struct {
		name string
		args args
		want []time.Time
	}{
		{name: "missing 1 date", args: struct {
			x db.TimeSeries
			y db.TimeSeries
		}{x: map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-02"): 1},
			y: map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-03"): 1}},
			want: []time.Time{toTime(t, "2020-01-01")},
		},
		{name: "all dates equal", args: struct {
			x db.TimeSeries
			y db.TimeSeries
		}{x: map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-03"): 1},
			y: map[time.Time]float64{toTime(t, "2020-01-01"): 1.2, toTime(t, "2020-01-03"): 1}},
			want: []time.Time{toTime(t, "2020-01-01"), toTime(t, "2020-01-03")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			SyncDates(tt.args.x, tt.args.y)
			var (
				lx []time.Time
				ly []time.Time
			)
			for k := range tt.args.x {
				lx = append(lx, k)
			}
			assert.Equal(t, tt.want, lx)
			for k := range tt.args.y {
				ly = append(ly, k)
			}
			assert.Equal(t, tt.want, ly)
		})
	}
}
