# PostgresVantage

This project consists of a docker container which has:
* a go client which polls regulary markdata from alphavantage.co
* it exposes a postgres database

For me this is the basis for any marketdata analysis.

I highly appreciate any pull requests.