package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/kelseyhightower/envconfig"

	"gitlab.com/BerndCzech/postgresvantage/internal/db"
)

func main() {
	var config db.Config
	// TODO: ENV vars are not found
	err := readEnv(&config)
	if err != nil {
		log.Fatal(err)
	}

	err = run(config)
	if err != nil {
		log.Fatal(err)
	}

}

func run(c db.Config) error {
	ctx, cancel := listenSigTerm(context.Background())
	defer cancel()

	d, closeDB, err := db.NewApp(context.Background(), c)
	if err != nil {
		return fmt.Errorf("could not initilize db %v", err)
	}
	defer closeDB()
	tick := time.Tick(20 * time.Second)
	wg := sync.WaitGroup{}
	for {
		select {
		case <-ctx.Done(): // probably shutdown
			break
		case <-tick:
			// API call 5 times per minute
		}
		wg.Add(1)
		err := d.Fill(ctx)
		if err != nil {
			switch e := err.(type) {
			case *db.TemporaryError:
				if e.RetryValid {
					log.Printf("temporary error: %+v, retry", err)
				}
				log.Fatalf("Application Error: \n %v \n", err)
			default:
				log.Fatalf("Application Error: \n %v \n", err)
			}
		}
		wg.Done()
	}
}

func readEnv(cfg *db.Config) error {
	return envconfig.Process("", cfg)
}

func listenSigTerm(ctx context.Context) (context.Context, context.CancelFunc) {

	appSignal := make(chan os.Signal, 1)
	signal.Notify(appSignal, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	ctx, cancel := context.WithCancel(ctx)
	go func() {
		select {
		case a := <-appSignal:
			log.Printf("got the signal to stop: %v \n", a)
			cancel()
		case <-ctx.Done():
			// someone else canceled
			break
		}
	}()
	return ctx, cancel
}
